﻿#include <iostream>
#include <string>
using namespace std;


int main()
{
    cout << "Please enter any line: ";
    string Str {};
    getline(cin, Str);
    cout << "You entered: " << '"' << Str << '"' << endl;
    cout << "String length = " << size(Str) << endl;
    cout << "First character of the string: " << Str[0] << endl;
    cout << "Last character of the string: " << Str[size(Str) - 1] << endl;
    return 0;
}


